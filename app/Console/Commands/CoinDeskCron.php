<?php

namespace App\Console\Commands;

use App\Models\CurrentPrice;
use Illuminate\Console\Command;
use App\Models\Currency;
use App\Models\CronLog;
use Curl;
class CoinDeskCron extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'coindesk:update';

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Cron runs every minute and pulled the bitcoin exchange rate using coindesk api and  update values in local database';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct()
	{
	parent::__construct();
	}

	/**
	* Execute the console command.
	*
	* @return void
	*/
	public function handle()
	{
		$start_time=date("Y-m-d H:i:s");

		$cron_log_id=CronLog::insertGetId(array('type'=>$this->signature,'completed'=>0,'start_time'=>$start_time));
		logScreen("Current Price update process started ".$start_time);

		$this->updatePrice();

		$end_time=date("Y-m-d H:i:s");

		logScreen("Current Price update process end ".$end_time);

		logScreen("Time taken to compete the update process ".(strtotime($end_time)-strtotime($start_time))." seconds",1);

		CronLog::where('id', $cron_log_id)
			->update(['completed' => 1,'end_time'=>$end_time]);
	}

	function getCurrencies()
	{

		$currencies = Currency::orderBy('order')->get();
		return $currencies;
	}

	function updatePrice()
	{
		//get all currencies
		$currencies=$this->getCurrencies();

		if($currencies)
		{
			$bulk_insert=array();
			$i=0;
			foreach ($currencies as $currency)
			{
				$currency_code=$currency->currency;


				$data=array();
				$response = json_decode(Curl::to(config('app.coindesk_api_url').'currentprice/'.$currency_code.'.json')->get());

				$data['updated_utc']=date("Y-m-d H:i:s",strtotime($response->time->updated));


				$data['rate']=$response->bpi->$currency_code->rate_float;

				$data['supported_currency_id']=$currency->id;



				//check atleast one record exist for current utc time. The reason we inserting as bulk
				//if yes then break  the proccess this might be duplicate request for the same min
				//if no then continue for next currency
				if($i==0)
				{
					$exist=CurrentPrice::where(array('updated_utc'=>$data['updated_utc'],'supported_currency_id'=>$data['supported_currency_id']))->first();
					if($exist)
					{
						break;
					}
				}

				$bulk_insert[]=$data;

				$i++;
			}
			//insert all data
			if(count($bulk_insert)>0)
				CurrentPrice::insert($bulk_insert);
		}
	}


}
