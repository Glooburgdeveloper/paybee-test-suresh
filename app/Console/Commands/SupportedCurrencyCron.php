<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Curl;

use App\Models\Currency;

class SupportedCurrencyCron extends Command
{
	/**
	* The name and signature of the console command.
	*
	* @var string
	*/
	protected $signature = 'supportedCurrency:update';

	/**
	* The console command description.
	*
	* @var string
	*/
	protected $description = 'Command description';

	/**
	* Create a new command instance.
	*
	* @return void
	*/
	public function __construct()
	{
		parent::__construct();
	}



	/**
	 * Execute the console command.
	 *
	 * @param $msg
	 * @param int $add_star
	 *
	 * @return void
	 */
	public function handle()
	{
		//get the list of supported currency from coin desk api
		logScreen("Fetching supported currencies from coin desk Api");
		$response = json_decode(Curl::to(config('app.coindesk_api_url').'supported-currencies.json')->get());
		if($response)
		{
			$i=0;
			$total=count($response);
			foreach ($response as $res)
			{
				//check row exist in db
				$exist = Currency::where('currency', $res->currency)->first();
				if(!$exist)
				{
					//insert the currency
					Currency::create(array('currency' =>  $res->currency,'country'=>$res->country,'order'=>99));
				}
				$i++;
				logScreen("Processing Currencies $i of $total");
			}

			logScreen("supported currencies imported successfully",1);
		}
		else
		{
			logScreen("Error Occurred While fetching supported currencies from coin desk Api",1);
		}

	}
}
