<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use File;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
	    Commands\CoinDeskCron::class,
	    Commands\SupportedCurrencyCron::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {


		//custom artisan command crondesk:cron created under commands
		//this will run every minyte to update the bitcoin exchange rate
		$schedule->command('coindesk:update')
				->name('coindesk-update')
				->cron('*/2 * * * * *');


		//custom artisan command crondesk:cron created under commands
		//this will run every minyte to update the bitcoin exchange rate
		$schedule->command('supportedCurrency:update')
				->name('supportedCurrency-cron')
				->hourly()
				->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
