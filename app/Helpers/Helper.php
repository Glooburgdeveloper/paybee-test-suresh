<?php

function logScreen($msg,$add_star=0)
{
	if($add_star)
		addStar($msg);

	echo $msg."\n";

	if($add_star)
		addStar($msg);
	else
		echo "\n";
}

function addStar($msg)
{
	echo str_repeat("*", (strlen($msg)+10))."\n";
}

?>