<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Models\Currency;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/bot-config';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
	    $data['phone_no']=phone($data['phone_no'], 'ZA','INTERNATIONAL');
        return Validator::make($data, [
	        'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
	        'phone_no' => 'required|phone:ZA,mobile|unique:users|',
            'email' => 'required|email|max:255|unique:users',
	        'password' => 'required|confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*([@!$#%])).+$/|',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
	    $data['name']= $data['first_name']." ". $data['last_name'];
        return User::create([
            'name' => $data['name'],
	        'first_name' => $data['first_name'],
	        'last_name' => $data['last_name'],
            'email' => $data['email'],
	        'phone_no'=>phone($data['phone_no'], 'ZA','INTERNATIONAL'),
	        'supported_currency_id'=>$this->getDefaultCurrency(),
            'password' => bcrypt($data['password']),
        ]);
    }

    function getDefaultCurrency()
    {

	    $currency_code=config('app.default_currency');

		$currency_data=(Currency::where('currency',$currency_code)->first());

	    if($currency_data)
	        return $currency_data->id;
		 else
		 	return 0;

    }
}
