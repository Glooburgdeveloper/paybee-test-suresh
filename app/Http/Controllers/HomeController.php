<?php

namespace App\Http\Controllers;


use App\Models\CurrentPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Currency;
use App\User;
use Auth;
use URL;
use Illuminate\Support\Facades\Redirect;
use App\Models\UserTelegram;
class HomeController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
	$this->middleware('auth');
	}

	/**
	* Show the application dashboard.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index()
	{
		$currencies=$this->getAllAvailableCurrencies();
		$telegram_info=$user_telegram=UserTelegram::where('user_id',Auth::user()->id)->get()->first();

		return view('home',array('currencies'=>$currencies,'telegram_info'=>$telegram_info));
	}

	function getAllAvailableCurrencies()
	{
		return Currency::orderBy('country')->get();
	}

	function saveDefaultCurrency(Request $request)
	{

		if($request->has('currency'))
		{
			$user_info = User::find(Auth::User()->id);

			$user_info->supported_currency_id =$request->input('currency');

			$user_info->save();

			return redirect()->route('botConfig');
		}
		else{
			return Redirect::back()->withInput()->withErrors(array('currency'=>'please select a valid currency'));
		}
	}

	function updateUser(Request $request)
	{
		$validator=$this->UserValidator($request->all());
		if($validator->fails())
		{
			return Redirect::to(URL::previous() . "#profile")->withInput()->withErrors($validator);

		}
		else
		{
			$user_info = User::find(Auth::User()->id);

			$user_info->name =$request->input('first_name')." ".$request->input('last_name');
			$user_info->first_name =$request->input('first_name');
			$user_info->last_name =$request->input('last_name');
			$user_info->email =$request->input('email');
			$user_info->phone_no =$request->input('phone_no');
			$user_info->password =$request->input('password');
			$user_info->password =$request->input('password');


			$user_info->save();

			return Redirect::to(URL::previous() . "#profile");
		}
	}

	protected function UserValidator(array $data)
	{

		$data['phone_no']=phone($data['phone_no'], 'ZA','INTERNATIONAL');
		$user_id = Auth::User()->id;

		return Validator::make($data, [
			'name' => 'required|max:255',
			'phone_no' => 'required|phone:ZA,mobile|unique:users,phone_no,'.$user_id.'|',
			'email' => 'required|email|max:255|unique:users,email,'.$user_id.'',
			'password' => 'required|confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*([@!$#%])).+$/|',
		]);
	}

	protected function PriceConverterValidator(array $data)
	{

		return Validator::make($data, [
			'amount' => 'required|numeric',
			'currency' => 'required|integer',
		]);
	}

	function getBTCEquivalent(Request $request)
	{
		$validator=$this->PriceConverterValidator($request->all());
		if($validator->fails())
		{
			return Redirect::to(URL::previous() . "#price")->withInput()->withErrors($validator);
		}
		else
		{

			$input_amount=(int)$request->input('amount');
			$input_currency_id=$request->input('currency');

			//eloquent method.We are not using eloquent this add extra complex layer for developer
			// and also  extra query to load every time join a table
			/*$data = CurrentPrice::with(['currency'])
				->where('updated_utc','>=','2016-12-16 19:24:00')
				->where('updated_utc','<=','2016-12-16 19:24:59')
				->where('supported_currency_id',$input_currency_id)
				->first();*/
			/*dump($data->currency->currency);
			dump($data->currency->country);
			dump($data->rate);
			dump($data->id);*/


			$data=$this->getCurrentPrice($input_currency_id);
			if($data)
			{
				$rate=(double)$data->rate;

				if($input_amount && $rate )
				{
					$btc=number_format(($input_amount/$rate),2);
					//reply format
					//30 USD is 0.08 BTC (760.45 USD - 1 BTC)
					$result="$input_amount ".$data->currency." is ".$btc."  BTC (".$rate." ".$data->currency." - 1 BTC )";

					return $this->successResponse($request,$result);
				}
				else
					return $this->failedResponse($request);
			}
			else
			{
				return $this->failedResponse($request,'Coin Desk Data Missing, Please try again');
			}

		}

	}

	function failedResponse($request,$msg='Input error occurred')
	{
		if ($request->ajax())
		{
			return response()->json([
				'error' => $msg
			], 401);
		}
		else
		{
			return Redirect::to(URL::previous() . "#price")->withInput()->withErrors(array('unknown_error'=>$msg));
		}
	}

	function successResponse($request,$msg='success',$data=array())
	{
		if ($request->ajax())
		{
			return response()->json([
				'success'=>true,
				'msg' => $msg,
				'data'=>$data
			], 200);
		}
		else
		{
			return Redirect::to(URL::previous() . "#price")->withInput()->with(array('exchange_rate_msg'=>$msg));
		}
	}


	function getCurrentPrice($input_currency_id)
	{
		//get the last updated record for past 5 mins.
		$start_date=date("Y-m-d H:i:59",strtotime("-5 minutes"));
		$end_date=date("Y-m-d H:i:s");

		$data=CurrentPrice::getCurrentPricesByCurrencyID($input_currency_id,$start_date,$end_date);
		return $data;
	}
}
