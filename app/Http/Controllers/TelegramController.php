<?php

namespace App\Http\Controllers;


use App\Models\UserTelegram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Currency;
use App\User;
use Auth;
use URL;
use Illuminate\Support\Facades\Redirect;
use Curl;
use Session;
use File;

/**
 * @property null MadelineProto


 * @property null user_phone_no
 * @property bool signup
 */
class TelegramController extends Controller
{
	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('auth');
		$this->MadelineProto=null;
		$this->user_phone_no=null;
		$this->signup=false;
		$this->setup();
	}



	function setup()
	{

		if(!isset($_SESSION)) session_start();

		if (isset($_SESSION['telegram_api_instance']))
		{
			$this->MadelineProto =unserialize($_SESSION['telegram_api_instance']);
		}

	}

	function saveObject()
	{
		$_SESSION['telegram_api_instance']=serialize($this->MadelineProto);
	}

	function start(Request $request)
	{

		if(!$this->MadelineProto)
			$this->MadelineProto = new \danog\MadelineProto\API();
		$user_phone_no=Auth::User()->phone_no;
		//remove plus from teh string
		$this->user_phone_no=str_replace("+","",$user_phone_no);
		//check this not registered
		$checkedPhone = $this->MadelineProto->auth->checkPhone( // auth.checkPhone becomes auth->checkPhone
			[
				'phone_number'     => $this->user_phone_no, // note that there should be no +
			]
		);
		//phone already registered
		if($checkedPhone['phone_registered']==false)
		{
			Session::put('telegram_signup',1);
			$this->signup=true;
		}
		//dump($checkedPhone);
		return $this->sendCode($request);

	}

	function sendCode($request)
	{
		$sentCode = $this->MadelineProto->phone_login($this->user_phone_no,0);
		$this->saveObject();
		if(isset($sentCode['phone_code_hash']))
		{
			return $this->successResponse($request,'success',array('signup'=>$this->signup));;
		}
		else
			return $this->failedResponse($request);;

	}

	function login(Request $request)
	{


		if($request->has('code'))
		{
			$code=$request->input('code');

			if(Session::get('telegram_signup')==1)
			{
				$first_name=Auth::User()->first_name;;
				$last_name=Auth::User()->last_name;
				$authorization = $this->MadelineProto->phone_signup($code,$first_name,$last_name);

				Session::put('telegram_signup',0);
			}
			else
				$authorization = $this->MadelineProto->complete_phone_login($code);

			if(isset($authorization['user']))
			{
				$this->addUserTelegram($authorization['user']['id']);
			}
			$this->saveObject();
			return $this->successResponse($request);
			//dd($authorization);
		}
	}

	function addUserTelegram($telegram_id)
	{
		return UserTelegram::updateOrCreate(array('user_id'=>Auth::User()->id),array('telegram_id'=>$telegram_id));
	}


	function failedResponse($request,$msg='Input error occurred')
	{
		if ($request->ajax())
		{
			return response()->json([
				'error' => $msg
			], 401);
		}

	}

	function successResponse($request,$msg='success',$data=array())
	{
		if ($request->ajax())
		{
			return response()->json([
				'success'=>true,
				'msg' => $msg,
				'data'=>$data
			], 200);
		}

	}



}
