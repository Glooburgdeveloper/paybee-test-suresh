<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class CronLog extends Model
{
    //
	protected $primaryKey = 'id';
	protected $table = 'cron_log';
	public $timestamps = true;
	protected $fillable = ['type', 'completed','start_time','end_time'];


}
