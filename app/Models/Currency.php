<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
	protected $primaryKey = 'id';
	protected $table = 'supported_currency';
	public $timestamps = true;
	protected $fillable = ['currency', 'country','order'];


}
