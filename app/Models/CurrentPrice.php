<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class CurrentPrice extends Model
{
    //
	protected $primaryKey = 'id';
	protected $table = 'current_price';
	public $timestamps = true;
	protected $fillable = ['supported_currency_id', 'rate','updated_utc','updated_iso','updated_uk'];

	public function currency()
	{
		return $this->belongsTo('App\Models\Currency','supported_currency_id');
	}

	public static function getCurrentPricesByCurrencyID($currency_id,$start_date,$end_date)
	{
		return DB::table('current_price')
			->join("supported_currency",'supported_currency.ID','=','current_price.supported_currency_id')
			->where('supported_currency_id', $currency_id)
			->where('updated_utc', '>=',$start_date)
			->where('updated_utc', '<=',$end_date)
			->orderby("updated_utc",'desc')
			->get()->first();
	}
}
