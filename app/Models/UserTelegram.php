<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class UserTelegram extends Model
{
    //
	protected $primaryKey = 'user_id';
	protected $table = 'user_telegram';
	public $timestamps = true;
	protected $fillable = ['user_id', 'telegram_id','telegram_class_object','telegram_user_object_expires_at'];


	static function getUserTelegramData($user_id,$expires_at)
	{
		return DB::table('user_telegram')
			->join("users",'users.id','=','user_telegram.user_id')
			->where('user_telegram.user_id',$user_id)
			->where('telegram_user_object_expires_at', '>',$expires_at)
			->get()->first();
	}
}
