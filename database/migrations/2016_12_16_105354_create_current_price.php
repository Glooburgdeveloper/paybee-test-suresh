<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrentPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('current_price', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('supported_currency_id');
			$table->double('rate');
			$table->dateTime('updated_utc');
			$table->timestamps();

		    $table->foreign('supported_currency_id')->references('id')->on('supported_currency');
	    });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('current_price');
    }
}
