<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrencyFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('users', function(Blueprint $table)
	    {
		    $table->integer('supported_currency_id')->after('phone_no');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
	    Schema::table('users', function(Blueprint $table)
	    {
		    $table->dropColumn('supported_currency_id');
	    });
    }
}
