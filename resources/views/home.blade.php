@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#default_currency">Default Currency</a></li>
            <li ><a data-toggle="tab" href="#profile">Profile</a></li>
            <li><a data-toggle="tab" href="#telegram">Telegram</a></li>
            <li><a data-toggle="tab" href="#price">Price Converter</a></li>
        </ul>

        <div class="tab-content">
            <div id="default_currency" class="tab-pane fade in active">
                <br>
                @include('inc/currency')
            </div>
            <div id="profile" class="tab-pane fade">
                <br>
                @include('inc/profile')
            </div>
            <div id="telegram" class="tab-pane fade">
                <br>
                @include('inc/telegram')
            </div>
            <div id="price" class="tab-pane fade">
                <br>
                @include('inc/price',['classAddition'=>''])
            </div>

        </div>
    </div>
</div>
@endsection
