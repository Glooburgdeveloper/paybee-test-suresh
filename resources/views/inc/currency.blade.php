
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/default-currency') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('currency') ? 'has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Default Currency</label>

                            <div class="col-md-6">
                                <select name="currency" id="currency" required class="form-control select">
                                    <option value="">Please select</option>
                                    @if(isset($currencies))
                                        @foreach($currencies as $currency)
                                             <option  @if($currency->id==Auth::User()->supported_currency_id) selected @endif value="{{$currency->id}}">{{$currency->country}}</option>
                                        @endforeach
                                    @endif
                                </select>

                                @if ($errors->has('currency'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>