
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">

                    <div id="unknown_error_text" class="@if (!$errors->has('unknown_error')) hide  @endif">
                        <div class="alert alert-warning">
                            {{ $errors->first('unknown_error') }}
                        </div>
                    </div>



                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/exchange-rate') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Amount</label>

                            <div class="col-md-6">
                                <input id="amount" step="any"  type="number" placeholder="200" class="form-control" name="amount"  value="{{old("amount")}}" required>

                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group{{ $errors->has('currency') ? 'has-error' : '' }}">
                            <label for="currency" class="col-md-4 control-label">Currency</label>
                            <div class="col-md-6">
                                <select name="currency" id="currency" required class="form-control select">
                                    <option value="">Please select</option>
                                    @if(isset($currencies))
                                        @foreach($currencies as $currency)
                                            @if(old("currency"))
                                             <option  @if($currency->id==old("currency")) selected @endif value="{{$currency->id}}">{{$currency->country}}</option>
                                            @else
                                                <option  @if($currency->id==Auth::user()->supported_currency_id) selected @endif value="{{$currency->id}}">{{$currency->country}}</option>
                                            @endif

                                        @endforeach
                                    @endif

                                </select>


                                @if ($errors->has('currency'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Convert
                                </button>
                            </div>
                        </div>
                    </form>
                        <div id="result_text" class="@if (!\Session::has('exchange_rate_msg')) hide  @endif">
                            <div class="alert alert-success ">
                                <ul>
                                    <li class="result">{!! \Session::get('exchange_rate_msg') !!}</li>
                                </ul>

                            </div>
                            <div >
                                    <h4 >Refreshing in  <span id="countdowntimer"></span></h4>
                            </div>
                        </div>


                </div>
            </div>
        </div>
    </div>
</div>

