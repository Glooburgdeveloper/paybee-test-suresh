
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body">
                    @if(!$telegram_info)
                        <div class="col-md-8 col-md-offset-5" id="register_oontainer" >
                            <button type="button" id="register_telegram" data-toggle="modal"
                                    data-target="#register_telegram_modal"  data-backdrop="static"
                                    data-keyboard="false"  aria-label="Close" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                     @else
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Telegram ID</label>
                            <div >
                               {{$telegram_info->telegram_id}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-md-4 control-label">Phone No</label>
                            <div >
                                {{Auth::User()->phone_no}}
                            </div>
                        </div>
                    @endif
                        <form class="form-horizontal hide" id="validate_code_form" role="form" method="POST" action="{{ url('/update-user') }}">
                            {{ csrf_field() }}
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Confirmation code</label>

                                <div class="col-md-6">

                                    <input id="code" type="number" class="form-control" name="code" value="" required autofocus>
                                    @if ($errors->has('code'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>

                                    @endif
                                    <span>
                                            Please enter the code received via  Telegram
                                        </span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>


                </div>


            </div>
        </div>
    </div>

    <div class="modal fade" id="register_telegram_modal"
         tabindex="-1" role="dialog"
         aria-labelledby="register_telegram_modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="register_telegram_modalLabel">Register Telegram</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Contacting
                        <b><span id="fav-title">Telegram Server.</span></b>
                        Please wait it might take few mins.
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>