<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/jquery.countdownTimer.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/js/jquery-countdownTimer-min.js"></script>

<script>
    $(function(){
        var hash = window.location.hash;
        hash && $('ul.nav a[href="' + hash + '"]').tab('show');

        $('.nav-tabs a').click(function (e) {
            $(this).tab('show');
            var scrollmem = $('body').scrollTop() || $('html').scrollTop();
            window.location.hash = this.hash;
            $('html,body').scrollTop(scrollmem);
        });

        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        })

        //count downtimer for auto refreshing
        initCountDownTimer();
        function initCountDownTimer()
        {
            var d = new Date();
            var sec = d.getSeconds();
            var timer_sec=(60-sec);

            $("#countdowntimer").countdowntimer({'minutes' : 1,'seconds':timer_sec,timeUp : timeisUp});
        }


        function timeisUp()
        {
            $.ajax({
                type: 'POST',
                url: '/exchange-rate',
                data:
                {
                    amount:$("#amount").val(),
                    currency:$("#currency").val(),
                },
                error: function(xhr, status, error)
                {
                    var data=JSON.parse(xhr.responseText);
                    if(data.error)
                    {
                        $("#result_text").fadeOut();
                        $("#unknown_error_text").removeClass('hide');
                        $("#unknown_error_text").find(".alert-warning").text(data.error);
                        $("#unknown_error_text").fadeIn();

                    }

                },

                success: function(data)
                {

                    $("#result_text").hide();
                    $("#unknown_error_text").hide();

                    if(data.success==true)
                    {
                        $("#result_text").removeClass('hide');
                        $("#result_text").find(".result").text(data.msg);
                        $("#result_text").fadeIn();

                        initCountDownTimer();
                    }
                    else
                    {
                        $("#unknown_error_text").removeClass('hide');
                        $("#unknown_error_text").find(".alert-warning").text(data.msg);
                        $("#unknown_error_text").fadeIn();


                    }

                }
            });
        }

        $("#register_telegram").click(function()
        {
       //success

            register();
        });

        function register()
        {
            $.ajax({
                type: 'GET',
                url: '/telegram',
                error: function(xhr, status, error)
                {
                    var data=JSON.parse(xhr.responseText);
                    if(data.error)
                    {
                        alert(data.error);
                        var ans=confirm("Do you want to try again");
                        if(ans)
                        {
                            register();

                        }
                    }
                    // alert('Error Occured While contacting telegram server. Please try again')
                    // window.location.reload();

                },

                success: function(data)
                {
                    showTelegramCode();
                }
            });
        }

        function showTelegramCode()
        {
            //$('#register_telegram_modal').hide();

            $("#register_oontainer").hide();
            $("#validate_code_form").removeClass('hide');
            $("#validate_code_form").fadeIn();
            $('#register_telegram_modal').modal('hide');

 //           setTimeout(function(){}, 3000);
        }
        $('#validate_code_form').submit(function(event) {
            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();

            $.ajax({
                type: 'POST',
                url: '/telegram/login',
                data:
                {
                    code:$("#code").val(),
                },
                error: function(xhr, status, error)
                {
                    var data=JSON.parse(xhr.responseText);
                    if(data.error)
                    {
                        alert(data.error);
                    }
                },

                success: function(data)
                {
                    window.location.reload();
                }
            });
        });

    });
</script>
</body>
</html>
