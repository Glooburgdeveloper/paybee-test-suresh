<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['XSS']], function ()
{
	Route::get('/', array('as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm'));

	Auth::routes();

	Route::get('/telegram', 'TelegramController@start');
	Route::get('/telegram/login', 'TelegramController@login');
	Route::post('/telegram/login', 'TelegramController@login');

	Route::get('/home', 'HomeController@index');

	Route::get('/default-currency', 'HomeController@index');

	Route::get('/bot-config', array('as'=>'botConfig','uses'=>'HomeController@index'));

	Route::post('/default-currency','HomeController@saveDefaultCurrency');

	Route::post('/update-user','HomeController@updateUser');

	Route::get('/exchange-rate', array('as'=>'botConfig','uses'=>'HomeController@index'));
	Route::post('/exchange-rate','HomeController@getBTCEquivalent');



});

